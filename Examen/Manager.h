#include <iostream>
#include <vector>
#include <time.h>
#include <chrono>
#include <unistd.h>
#include <algorithm>

using namespace std;
using namespace chrono;
class Manager
{
    private:

    void ExecuteMergeSort();
    void ExecuteHanoiTower();
    void ExecuteFibonacci();

    void MergeSortList();
    void HanoiTowerList();
    void FibonacciList();

    void merge(int *array, int p, int q, int r);    
    void merge_sort(int *array, int p, int r);

    void HanoiTower(int num, char a, char b, char c);
    int Fibonacci(int n);

    public:
    Manager();
    void Test();
};