#include "Manager.h"
Manager::Manager()
{
    srand (time(NULL));
}

void Manager::ExecuteMergeSort()
{
    this->MergeSortList();
}

void Manager::ExecuteHanoiTower()
{
    this->HanoiTowerList();
}

void Manager::ExecuteFibonacci()
{
    this->FibonacciList();
}

void Manager::Test()
{
    this->ExecuteMergeSort();
    this->ExecuteHanoiTower();
    this->ExecuteFibonacci();
}

void Manager::HanoiTowerList()
{
    
    int n1 = 10,n2 = 7,n3 = 8,n4 = 9,n5 = 12;
    int t1,t2,t3,t4,t5;

    steady_clock::time_point start,end;
    start = steady_clock::now();
    HanoiTower(n1, 'A', 'B', 'C');
    end = steady_clock::now();
    t1 = duration_cast<nanoseconds>(end - start).count();

    start = steady_clock::now();
    HanoiTower(n2, 'A', 'C', 'D');
    end = steady_clock::now();
    t2 = duration_cast<nanoseconds>(end - start).count();

    start = steady_clock::now();
    HanoiTower(n3, 'M', 'J', 'A');
    end = steady_clock::now();
    t3 = duration_cast<nanoseconds>(end - start).count();

    start = steady_clock::now();
    HanoiTower(n4, 'A', 'B', 'C');
    end = steady_clock::now();
    t4 = duration_cast<nanoseconds>(end - start).count();

    start = steady_clock::now();
    HanoiTower(n5, 'A', 'Z', 'H');
    end = steady_clock::now();
    t5 = duration_cast<nanoseconds>(end - start).count();
    
    
    cout<<"<<<<<<<<<<<<<<<< TORRES DE HANOI >>>>>>>>>>>>>>>>>>>>>"<<endl;
    cout<<"Tiempos y resultado:"<<endl;
    cout<<"Llamado 1:"<<endl;
    cout<<"Valor:"<<n1<<endl;
    cout<<"Tiempo:"<<t1<<endl;
    cout<<endl;
    
    cout<<"Llamado 2:"<<endl;
    cout<<"Valor:"<<n2<<endl;
    cout<<"Tiempo:"<<t2<<endl;
    cout<<endl;

    cout<<"Llamado 3:"<<endl;
    cout<<"Valor:"<<n3<<endl;
    cout<<"Tiempo:"<<t3<<endl;
    cout<<endl;

    cout<<"Llamado 4:"<<endl;
    cout<<"Valor:"<<n4<<endl;
    cout<<"Tiempo:"<<t4<<endl;
    cout<<endl;

    cout<<"Llamado 5:"<<endl;
    cout<<"Valor:"<<n5<<endl;
    cout<<"Tiempo:"<<t5<<endl;
    cout<<endl;
    cout<<"<<<<<<<<<<<<<<<< TORRES DE HANOI >>>>>>>>>>>>>>>>>>>>>"<<endl;
}

void Manager::FibonacciList()
{
    int n1 = 5;
    int n2 = 7;
    int n3 = 9;
    int n4 = 13;
    int n5 = 20;
    
    
    int t1,t2,t3,t4,t5;

    steady_clock::time_point start,end;
    start = steady_clock::now();
    int sum1 = Fibonacci(n1);
    end = steady_clock::now();
    t1 = duration_cast<nanoseconds>(end - start).count();

    start = steady_clock::now();
    int sum2 = Fibonacci(n2);
    end = steady_clock::now();
    t2 = duration_cast<nanoseconds>(end - start).count();

    start = steady_clock::now();
    int sum3 = Fibonacci(n3);
    end = steady_clock::now();
    t3 = duration_cast<nanoseconds>(end - start).count();

    start = steady_clock::now();
    int sum4 = Fibonacci(n4);
    end = steady_clock::now();
    t4 = duration_cast<nanoseconds>(end - start).count();

    start = steady_clock::now();
    int sum5 = Fibonacci(n5);
    end = steady_clock::now();
    t5 = duration_cast<nanoseconds>(end - start).count();

    cout<<"<<<<<<<<<<<<<<<< FIBONACCI >>>>>>>>>>>>>>>>>>>>>"<<endl;
    cout<<"Tiempos y resultado:"<<endl;
    cout<<"Llamado 1:"<<endl;
    cout<<"Valor:"<<n1<<endl;
    cout<<"Tiempo:"<<t1<<endl;
    cout<<"Resultado:"<<sum1<<endl;
    cout<<endl;

    cout<<"Llamado 2:"<<endl;
    cout<<"Valor:"<<n2<<endl;
    cout<<"Tiempo:"<<t2<<endl;
    cout<<"Resultado:"<<sum2<<endl;
    cout<<endl;

    cout<<"Llamado 3:"<<endl;
    cout<<"Valor:"<<n3<<endl;
    cout<<"Tiempo:"<<t3<<endl;
    cout<<"Resultado:"<<sum3<<endl;
    cout<<endl;

    cout<<"Llamado 4:"<<endl;
    cout<<"Valor:"<<n4<<endl;
    cout<<"Tiempo:"<<t4<<endl;
    cout<<"Resultado:"<<sum4<<endl;
    cout<<endl;

    cout<<"Llamado 5:"<<endl;
    cout<<"Valor:"<<n5<<endl;
    cout<<"Tiempo:"<<t5<<endl;
    cout<<"Resultado:"<<sum5<<endl;
    cout<<endl;
    cout<<"<<<<<<<<<<<<<<<< FIBONACCI >>>>>>>>>>>>>>>>>>>>>"<<endl;
}

void Manager::MergeSortList()
{
    int A[] = {2, 4, 5, 7, 1, 1000, 3, 6};
    int rA = sizeof(A)/sizeof(A[0]) - 1;
    
    int B[] = {78, 24, 35, 17, 51, 62, 33, 06};
    int rB = sizeof(B)/sizeof(B[0]) - 1;
    
    int C[] = {22, 34, 25, 17, 41, 52, 638, 86};
    int rC = sizeof(C)/sizeof(C[0]) - 1;
    
    int D[] = {24, 43, 52, 71, 17, 26, 33, 64};
    int rD = sizeof(D)/sizeof(D[0]) - 1;
    
    int E[] = {29, 49, 56, 74, 13, 22, 32, 26};
    int rE = sizeof(E)/sizeof(E[0]) - 1;

    int t1,t2,t3,t4,t5;

    steady_clock::time_point start,end;
    start = steady_clock::now();
    merge_sort(A,0,rA);  
    end = steady_clock::now();
    t1 = duration_cast<nanoseconds>(end - start).count();

    start = steady_clock::now();
    merge_sort(B,0,rB);
    end = steady_clock::now();
    t2 = duration_cast<nanoseconds>(end - start).count();

    
    start = steady_clock::now();
    merge_sort(C,0,rC);
    end = steady_clock::now();
    t3 = duration_cast<nanoseconds>(end - start).count();

    
    start = steady_clock::now();
    merge_sort(D,0,rD);
    end = steady_clock::now();
    t4 = duration_cast<nanoseconds>(end - start).count();


    start = steady_clock::now();
    merge_sort(E,0,rE);
    end = steady_clock::now();
    t5 = duration_cast<nanoseconds>(end - start).count();

    cout<<"<<<<<<<<<<<<<<<< MERGE SORT >>>>>>>>>>>>>>>>>>>>>"<<endl;
    cout<<"Arreglo 1 ordenado:"<<endl;
    for (int i = 0; i < rA + 1; i++)
    {
        printf(" %i ", *(A + i));
    }
    cout<<endl;
    cout<<"Arreglo 2 ordenado:"<<endl;
    for (int i = 0; i < rB + 1; i++)
    {
        printf(" %i ", *(B + i));
    }
    cout<<endl;
    cout<<"Arreglo 3 ordenado:"<<endl;
    for (int i = 0; i < rC + 1; i++)
    {
        printf(" %i ", *(C + i));
    }
    cout<<endl;
    cout<<"Arreglo 4 ordenado:"<<endl;
    for (int i = 0; i < rD + 1; i++)
    {
        printf(" %i ", *(D + i));
    }
    cout<<endl;
    cout<<"Arreglo 5 ordenado:"<<endl;
    for (int i = 0; i < rE + 1; i++)
    {
        printf(" %i ", *(E + i));
    }
    cout<<endl;
    cout<<endl;

    cout<<"Tiempos(nanoseconds)"<<endl;
    cout<<"Arreglo 1:"<<t1<<endl;
    cout<<"Arreglo 2:"<<t2<<endl;
    cout<<"Arreglo 3:"<<t3<<endl;
    cout<<"Arreglo 4:"<<t4<<endl;
    cout<<"Arreglo 5:"<<t5<<endl;
    cout<<"<<<<<<<<<<<<<<<< MERGE SORT >>>>>>>>>>>>>>>>>>>>>"<<endl;
}

void Manager::merge(int *array, int p, int q, int r)
{
    int i, j, k;
    int n1 = (q - p) + 1;
    int n2 = (r - q);
    int *L, *R;

    L = (int*)malloc(n1 * sizeof(int));
    R = (int*)malloc(n2 * sizeof(int));


    for (i = 0; i < n1; i++)
    {
        L[i] = *(array + p + i);
    }

    for (j = 0; j < n2; j++)
    {
        R[j] = *(array + q + j + 1);
    }

    i = 0;
    j = 0;

    // Combinacion de datos
    for (k = p; k < r + 1; k++)
    {
        if (i == n1)
        {
            *(array + k) = *(R + j);
            j =  j+ 1;
        }
        else if(j == n2)
        {
            *(array + k) = *(L + i);
            i = i + 1;
        }
        else
        {
            if (*(L + i) <= *(R + j))
            {
                *(array + k) = *(L + i);
                i = i + 1;
            }
            else
            {
                *(array + k) = *(R + j);
                j = j + 1;
            }
        }
    }
}

void Manager::merge_sort(int *array, int p, int r)
{
    if (p < r)
    {
        // Divide, dividiendolo en subproblemas
        int q = (p + r)/2;
        
        // Resolucion recursiva para el conquista
        merge_sort(array, p, q);
        merge_sort(array, q + 1, r);
        
        // Union
        merge(array, p, q, r);
    }
}

 void Manager::HanoiTower(int num, char a, char b, char c) 
 {
		// Si solo hay un disco
		if(num == 1) {
			//cout<<"Primer disco desde "<<a<<"->"<<c<<endl;
		} else {
			HanoiTower(num - 1, a, c, b);
            //cout<<" "<<num<<"Un disco de"<<a<<"->"<<c<<endl;
			HanoiTower(num - 1, b, a, c);			
		}
}

int Manager::Fibonacci(int n) {
      //caso base
      if (n == 1 || n == 2)
         return 1;
      else
      {
        //Se divide en subproblemas
        int result = Fibonacci(n - 1) + Fibonacci(n - 2);
        return  result;
      }
}